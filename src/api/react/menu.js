import request from '@/utils/request'

/** 3d平台菜单管理*/

// 获取单条菜单
export const getMenu = (params) => {
  return request.get('/api/react/menu/get', { params })
}
// 获取全部菜单
export const getMenuList = (params) => {
  return request.get('/api/react/menu/getlist', { params })
}
// 获取分页菜单信息
export const getMenuListPage = (params) => {
  params = params || {}
  return request.post('/api/react/menu/getpage', params)
}
// 添加菜单
export const addMenu = (params) => {
  return request.post('/api/react/menu/add', params)
}
// 修改菜单
export const editMenu = (params) => {
  return request.put('/api/react/menu/update', params)
}
// 删除菜单
export const removeMenu = (params) => {
  return request.delete('/api/react/menu/softdelete', { params })
}
// 批量删除菜单
export const batchRemoveMenu = (params) => {
  return request.put('/api/react/menu/BatchsoftDelete', params)
}
